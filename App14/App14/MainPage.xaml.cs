﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace App14
{
    public partial class MainPage : ContentPage
    {
        private System.Timers.Timer timer;

        private BackgroundWorker backgroundWorker1 = new BackgroundWorker();

        public MainPage()
        {
            InitializeComponent();

            timer = new System.Timers.Timer();
            timer.Interval = 3000; //타이머 3초 단위로 실행
            timer.Elapsed += timer_Elapsed;
            timer.Start(); //타이머 시작

            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //ToDo
            //GPS수신 완료
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //ToDo
            Debug.WriteLine("GPS 수신 시작");
            
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    //https://docs.microsoft.com/ko-kr/xamarin/essentials/geolocation?tabs=android
                    //2가지 방법이 있는데 어느거로 할건지 테스트해서 선택

                    //var location = await Geolocation.GetLastKnownLocationAsync();

                    var request = new GeolocationRequest(GeolocationAccuracy.Medium); //설정 가능 Medium, High
                    var location = await Geolocation.GetLocationAsync(request);

                    if (location != null)
                    {
                        Debug.WriteLine("===== GPS ======");
                        Debug.WriteLine($"Latitude: {location.Latitude}, Longitude: {location.Longitude}, Altitude: {location.Altitude}");
                    }
                    else
                    {
                        Debug.WriteLine("===== location is NULL ======");
                    }
                });
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                Debug.WriteLine(fnsEx.Message);
            }
            catch (PermissionException pEx)
            {
                Debug.WriteLine(pEx.Message);
            }
            catch (Exception ex)
            {
                // Unable to get location
                Debug.WriteLine(ex.Message);
            }
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //타이머가 실행되고 있는거 확인, 현재 시간 출력
            //특정 시간에만 실행되게 하려면 e.SignalTime과 비교하여 처리한다.
            Console.WriteLine("Timer Working {0} ", e.SignalTime.ToString());

            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }
            else
            {
                Debug.WriteLine("GPS 수신중....");
            }
        }

        private void BtnStop_Clicked(object sender, EventArgs e)
        {
            timer.Stop();
        }

        private void BtnStart_Clicked(object sender, EventArgs e)
        {
            timer.Start();
        }
    }
}
